﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wyświetlacz.Kontrakt
{
    public interface IWyswietlacz
    {
        void Wyswietl(string wiadomosc);
        void Reset();
    }
}
