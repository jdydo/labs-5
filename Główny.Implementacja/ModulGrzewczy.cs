﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Główny.Kontrakt;
using Wyświetlacz.Implementacja;
using Wyświetlacz.Kontrakt;

namespace Główny.Implementacja
{
    public class ModulGrzewczy : IModulGrzewczy
    {
        IWyswietlacz wyswietlacz;

        public ModulGrzewczy(IWyswietlacz wyswietlacz)
        {
            this.wyswietlacz = wyswietlacz;
        }

        void IModulGrzewczy.WlaczOgrzewanie()
        {
            wyswietlacz.Wyswietl("Włącz ogrzewanie");
        }

        void IModulGrzewczy.WylaczOgrzewanie()
        {
            wyswietlacz.Wyswietl("Wyłącz ogrzewanie");
        }
    }
}
