﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MExpectedException = Microsoft.VisualStudio.TestTools.UnitTesting.ExpectedExceptionAttribute;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;
using ExpectedException = NUnit.Framework.ExpectedExceptionAttribute;
using PK.Container;
using Lab5.Infrastructure;
using System.Reflection;
using System.Collections.Generic;
using Lab5.Main;

namespace Lab5.Test
{
    [TestFixture]
    [TestClass]
    public class P2_Components
    {
        [Test]
        [TestMethod]
        public void P2__Main_Component_Contract_Library_Should_Exist()
        {
            // Arrange
            var assembly = LabDescriptor.MainComponentSpec;

            // Assert
            Assert.That(assembly, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P2__Main_Component_Library_Should_Exist()
        {
            // Arrange
            var assembly = LabDescriptor.MainComponentImpl;

            // Assert
            Assert.That(assembly, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P2__Display_Component_Contract_Library_Should_Exist()
        {
            // Arrange
            var assembly = LabDescriptor.DisplayComponentSpec;

            // Assert
            Assert.That(assembly, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P2__Display_Component_Library_Should_Exist()
        {
            // Arrange
            var assembly = LabDescriptor.DisplayComponentImpl;

            // Assert
            Assert.That(assembly, Is.Not.Null);
        }

        [Test]
        [TestMethod]
        public void P2__Contract_And_Component_Main_Libraries_Should_Be_Different()
        {
            // Arrange
            var contract = LabDescriptor.MainComponentSpec;
            var component = LabDescriptor.MainComponentImpl;

            // Assert
            Assert.That(contract, Is.Not.EqualTo(component));
        }

        [Test]
        [TestMethod]
        public void P2__Contract_And_Component_Display_Libraries_Should_Be_Different()
        {
            // Arrange
            var contract = LabDescriptor.DisplayComponentSpec;
            var component = LabDescriptor.DisplayComponentImpl;

            // Assert
            Assert.That(contract, Is.Not.EqualTo(component));
        }

        [Test]
        [TestMethod]
        public void P2__Main_Contract_Library_Should_Not_Depend_On_Other_Components_Or_Frameworks()
        {
            // Arrange
            var assembly = LabDescriptor.MainComponentSpec;
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib"));
        }

        [Test]
        [TestMethod]
        public void P2__Main_Component_Library_Should_Not_Depend_On_Other_Components_Or_Frameworks()
        {
            // Arrange
            var assembly = LabDescriptor.MainComponentImpl;
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib")
                                        .Or.Property("Name").EqualTo(LabDescriptor.MainComponentSpec.GetName().Name)
                                        .Or.Property("Name").EqualTo(LabDescriptor.DisplayComponentSpec.GetName().Name));
        }

        [Test]
        [TestMethod]
        public void P2__Display_Contract_Library_Should_Not_Depend_On_Other_Components_Or_Frameworks()
        {
            // Arrange
            var assembly = LabDescriptor.DisplayComponentSpec;
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib"));
        }

        [Test]
        [TestMethod]
        public void P2__Display_Component_Library_Should_Not_Depend_On_Other_Components_Or_Frameworks()
        {
            // Arrange
            var assembly = LabDescriptor.DisplayComponentImpl;
            var referenced = assembly.GetReferencedAssemblies();

            // Assert
            Assert.That(referenced, Has.All.Property("Name").StartsWith("System")
                                        .Or.Property("Name").StartsWith("Microsoft")
                                        .Or.Property("Name").EqualTo("mscorlib")
                                        .Or.Property("Name").EqualTo("WindowsBase")
                                        .Or.Property("Name").EqualTo("PresentationCore")
                                        .Or.Property("Name").EqualTo("PresentationFramework")
                                        .Or.Property("Name").EqualTo("Lab5.DisplayForm")
                                        .Or.Property("Name").EqualTo(LabDescriptor.DisplayComponentSpec.GetName().Name));
        }

        [Test]
        [TestMethod]
        public void P2__Main_Contract_Library_Should_Contain_Only_Interfaces_Or_Enums()
        {
            // Arrange
            var assembly = LabDescriptor.MainComponentSpec;
            var types = assembly.GetTypes();
            var i = types[0].IsInterface;

            // Assert
            foreach (var type in types)
                Assert.That(type.IsInterface || type.IsEnum);
        }

        [Test]
        [TestMethod]
        public void P2__Display_Contract_Library_Should_Contain_Only_Interfaces_Or_Enums()
        {
            // Arrange
            var assembly = LabDescriptor.DisplayComponentSpec;
            var types = assembly.GetTypes();
            var i = types[0].IsInterface;

            // Assert
            foreach (var type in types)
                Assert.That(type.IsInterface || type.IsEnum);
        }

        [Test]
        [TestMethod]
        public void P2__Main_Component_Should_Not_Contain_Other_Public_Classes()
        {
            // Arrange
            var assembly = LabDescriptor.MainComponentImpl;
            var types = assembly.GetExportedTypes();
            var interfaces = new HashSet<Type>(LabDescriptor.MainComponentSpec.GetTypes());

            // Assert
            Assert.That(types, Is.Not.Null);
            Assert.That(types, Is.Not.Empty);
            foreach (var type in types)
            {
                var ifaces = type.GetInterfaces();
                Assert.That(ifaces, Is.Not.Null);
                Assert.That(ifaces, Is.Not.Empty);
                Assert.That(ifaces, Has.All.Matches<Type>(iface => interfaces.Contains(iface)));
            }
        }

        [Test]
        [TestMethod]
        public void P2__Display_Component_Should_Not_Contain_Other_Public_Classes()
        {
            // Arrange
            var assembly = LabDescriptor.DisplayComponentImpl;
            var types = assembly.GetExportedTypes();
            var interfaces = new HashSet<Type>(LabDescriptor.DisplayComponentSpec.GetTypes());

            // Assert
            Assert.That(types, Is.Not.Null);
            Assert.That(types, Is.Not.Empty);
            foreach (var type in types)
            {
                var ifaces = type.GetInterfaces();
                Assert.That(ifaces, Is.Not.Null);
                Assert.That(ifaces, Is.Not.Empty);
                Assert.That(ifaces, Has.All.Matches<Type>(iface => interfaces.Contains(iface)));
            }
        }

        [Test]
        [TestMethod]
        [MExpectedException(typeof(UnresolvedDependenciesException))]
        [ExpectedException(typeof(UnresolvedDependenciesException))]
        public void P2__Main_Component_Should_Depend_On_Display_Component()
        {
            // Arrange
            var container = (IContainer)Activator.CreateInstance(LabDescriptor.Container);
            var iface = LabDescriptor.MainComponentSpec.GetTypes()[0];

            // Act
            container.Register(LabDescriptor.MainComponentImpl);
            var result = container.Resolve(iface);
        }

        [Test]
        [TestMethod]
        public void P2__Container_Should_Be_Properly_Configured_In_ConfigureApp()
        {
            // Arrange
            var container = Configuration.ConfigureApp();
            var iface = LabDescriptor.MainComponentSpec.GetTypes()[0];

            // Act
            Program.ApplicationStart();
			System.Threading.Thread.Sleep(500);
            container.Register(LabDescriptor.MainComponentImpl);
            var result = container.Resolve(iface);
            Program.ApplicationStop();

            // Assert
            Assert.That(result, Is.Not.Null);
        }
    }
}
