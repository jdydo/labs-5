﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab5.DisplayForm;
using System.Windows;
using Wyświetlacz.Kontrakt;

namespace Wyświetlacz.Implementacja
{
    public class Wyswietlacz : IWyswietlacz
    {
        DisplayViewModel model;

        public Wyswietlacz()
        {
            model = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
            {
                // utworzenie nowej formatki graficznej stanowiącej widok
                var form = new Form();
                // utworzenie modelu widoku (wzorzec MVVM)
                var viewModel = new DisplayViewModel();
                // przypisanie modelu do widoku
                form.DataContext = viewModel;
                // wyświetlenie widoku
                form.Show();
                // zwrócenie modelu widoku do dalszych manipulacji
                return viewModel;
            }), null);
        }

        void IWyswietlacz.Wyswietl(string wiadomosc)
        {
            model.Text = wiadomosc;
        }

        void IWyswietlacz.Reset()
        {
            model.Text = "";
        }
    }
}
