﻿using System;
using PK.Container;
using System.Reflection;


namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            Container container = new Container();
            container.Register(Assembly.Load("Główny.Implementacja"));
            container.Register(Assembly.Load("Główny.Kontrakt"));
            container.Register(Assembly.Load("Wyświetlacz.Implementacja"));
            container.Register(Assembly.Load("Wyświetlacz.Kontrakt"));
            return container;
        }
    }
}
