﻿using System;
using System.Reflection;
using Główny.Kontrakt;
using Główny.Implementacja;
using Wyświetlacz.Kontrakt;
using Wyświetlacz.Implementacja;
using PK.Container;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Container);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IModulGrzewczy));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(ModulGrzewczy));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(IWyswietlacz));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Wyswietlacz));

        #endregion
    }
}
