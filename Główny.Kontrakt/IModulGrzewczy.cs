﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Główny.Kontrakt
{
    public interface IModulGrzewczy
    {
        void WlaczOgrzewanie();
        void WylaczOgrzewanie();
    }
}
